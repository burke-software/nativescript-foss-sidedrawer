# 2.1.0

- Added `headerTextColor` option.

# 2.0.0

- Nativescript 6.0 compatibility. Use 1.x for Nativescript <6.0.
- com.mikepenz:materialdrawer is updated to 6.1.2 and supports androidx
- Updated tns-core-modules imports !1
