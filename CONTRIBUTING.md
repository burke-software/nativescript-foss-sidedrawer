# Contributing to Nativescript FOSS Sidedrawer

# Running the demo

This project is based on [nativescript-plugin-seed](https://github.com/NativeScript/nativescript-plugin-seed).

To get the demo up and running just do

1. `cd src`
2. `npm run demo.android` (or ios)

This should launch a demo app. Just pull the sidedrawer in from the left of the screen to view it.
